/*
    Name: GetTableTypeInfo.sql
    Description: Returns a list of all table types and they related info
    Keywords: table types
    Ref: 
*/

SELECT
	USER_NAME(a.schema_id) AS 'Username',
	a.name AS "Type Name",
    b.column_id AS 'Column ID',
    SUBSTRING(CAST(b.column_id + 100 AS CHAR(3)), 2, 2)  + ': ' + b.name AS 'Column',
    c.name AS 'Data Type',
    b.Is_Nullable AS 'Nullable',
    b.max_length AS 'Length',
    b.[precision] AS 'Precision',
    b.scale AS 'Scale',
    c.collation AS 'Collation'
FROM 
	sys.table_types a 
	JOIN 
	sys.columns b ON a.type_table_object_id = b.object_id 
	JOIN 
	sys.systypes c  ON c.xtype = b.system_type_id
WHERE
	a.is_user_defined = 1
ORDER BY 
	a.name,
    b.column_id