/*
    Name: LastModifiedObjects.sql
    Description: This script will list all modified objects from a DB using a date range and object type as filters
    Keywords: 
*/

DECLARE 
@StartDate DATETIME = N'2018-05-14 00:00:00.000',
@OrderOption BIT = 1,
@FilterOption VARCHAR(3) = N''

-- https://docs.microsoft.com/en-us/sql/relational-databases/system-catalog-views/sys-objects-transact-sql

SELECT
	b.name as [schema],
	a.name,
	a.type,
	a.type_desc,
	a.modify_date
INTO
	#records
FROM
	sys.objects a
	JOIN
	sys.schemas b on b.schema_id = a.schema_id
WHERE
	a.modify_date BETWEEN @StartDate AND GETDATE()
	AND
	a.type IN ('FN','P','TR','TT','V','U')
ORDER BY
	a.type,
	a.modify_date DESC

-- 1 = By Modify Date
-- 0 = Schema/Name
IF(@OrderOption = 1)
BEGIN
	IF(@FilterOption <> '')
	BEGIN
		SELECT * FROM #records WHERE type = @FilterOption ORDER BY modify_date DESC
	END
	ELSE
	BEGIN
		SELECT * FROM #records WHERE type = 'P' ORDER BY modify_date DESC
		SELECT * FROM #records WHERE type = 'U' ORDER BY modify_date DESC
		SELECT * FROM #records WHERE type = 'V' ORDER BY modify_date DESC
		SELECT * FROM #records WHERE type = 'TT' ORDER BY modify_date DESC
		SELECT * FROM #records WHERE type = 'FN' ORDER BY modify_date DESC
	END
END
ELSE
BEGIN
	IF(@FilterOption <> '')
	BEGIN
		SELECT * FROM #records WHERE type = @FilterOption ORDER BY [schema],name
	END
	ELSE
	BEGIN
		SELECT * FROM #records WHERE type = 'P' ORDER BY [schema],name
		SELECT * FROM #records WHERE type = 'U' ORDER BY [schema],name
		SELECT * FROM #records WHERE type = 'V' ORDER BY [schema],name
		SELECT * FROM #records WHERE type = 'TT' ORDER BY [schema],name
		SELECT * FROM #records WHERE type = 'FN' ORDER BY [schema],name
	END
END

DROP TABLE #records