/*
    Name: CharAndWordCounter.sql
    Description: Returns the number of occurrences of a word or char in a string
    Keywords: char, word, counter
*/

DECLARE
@text NVARCHAR(MAX) = N'',       -- The expression to be evaluated
@charFilter CHAR(1) = N'',      -- The char to filter by
@wordFilter NVARCHAR(MAX) = N'', -- The word to filter by
@charCount INT,                 -- A variable to count the chars found
@wordCount INT,                 -- A variable to count the words found
@operationType BIT              -- The type of operation to run

IF(DATALENGTH(@text) > 0)
BEGIN
	IF(@operationType = 0)
	BEGIN
		SET @charCount = (SELECT DATALENGTH(@text) - DATALENGTH(REPLACE(@text,@charFilter,'')))
		SELECT @charCount
	END
	ELSE
	BEGIN
		SET @wordCount = (SELECT DATALENGTH(@text) - (DATALENGTH(REPLACE(@text,@wordFilter,'')) / DATALENGTH(@wordFilter)))
		SELECT @wordCount
	END
END