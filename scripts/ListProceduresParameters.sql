/*
    Name: ListProceduresParameters.sql
    Description: Returns a list of procedures and which parameters they use
    Keywords: stored procedures, parameters
    Ref: 
*/

SELECT
	'SchemaID'      = b.schema_id,
	'ProcedureID'   = b.object_id,
	'SchemaName'    = SCHEMA_NAME(b.schema_id),
	'ProcedureName' = b.name,
	'ParameterName' = a.name,
	'DataType'      = TYPE_NAME(a.user_type_id),
	'Length'        = a.max_length,
	'Prec'          = CASE WHEN TYPE_NAME(a.system_type_id) = 'uniqueidentifier' THEN PRECISION ELSE ODBCPREC(a.system_type_id, a.max_length, PRECISION) END,
	'Scale'         = ODBCSCALE(a.system_type_id, a.scale),
	'Param_order'   = a.parameter_id,
	'Collation'     = CONVERT(sysname, CASE WHEN a.system_type_id IN (35, 99, 167, 175, 231, 239) THEN SERVERPROPERTY('collation') END)
FROM
	sys.parameters a
	join
	sys.procedures b on b.object_id = a.object_id
ORDER BY
	b.name