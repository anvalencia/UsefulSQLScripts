/*
    Name: CreateAuditTable.sql
    Description: Creates an audit table to catch errors inside stored procedures within a DB
    Keywords: audit, log
*/

-- Replace DB_NAME with the actual name of the DB
USE [DB_NAME]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- Check if Audit schema exists, if not, create it (if permissions are defined)
if not exists(select schema_name from information_schema.schemata where schema_name = 'Audit')
begin
    exec sp_executesql N'CREATE SCHEMA Audit'
end
else
begin
    if(object_id(N'Audit.ProcedureErrors', N'U') is not null)
    begin
        create table [Audit].[ProcedureErrors]
        (
            [ErrorID] [uniqueidentifier] NOT NULL
            ,[AppLogID] [uniqueidentifier] null
            ,[ErrorNumber] [int] NOT NULL
            ,[ErrorSeverity] [int] NOT NULL
            ,[ErrorState] [int] NOT NULL
            ,[ErrorLine] [int] NOT NULL
            ,[ErrorMessage] [nvarchar](max) NULL
            ,[ProcedureName] [nvarchar](max) NULL
            ,[Method] [varchar](258) NULL
            ,[SqlStatement] [nvarchar](max) NULL
            ,[ErrorFixed] [bit] NULL
            ,[RaisedOn] [datetime] NOT NULL
            ,CONSTRAINT [PK_ProcedureErrors] PRIMARY KEY CLUSTERED
            (
                [ErrorID] ASC
            ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) on [PRIMARY] TEXTIMAGE_ON [PRIMARY]

        alter table [Audit].[ProcedureErrors] ADD CONSTRAINT [DF_ProcedureErrors_ErrorID] DEFAULT (newid()) FOR [ErrorID]

        alter table [Audit].[ProcedureErrors] ADD CONSTRAINT [DF_ProcedureErrors_RaisedOn] DEFAULT (getdate()) FOR [RaisedOn]
    end
end