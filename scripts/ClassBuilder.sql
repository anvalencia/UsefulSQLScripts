/*
    Name: ClassBuilder.sql
    Description: Generates a set of data values needed to create a class structire in C# by using a table and validating the data types for each column field
    Keywords: tables, code, object, structure, data types
    Ref: 
*/

DECLARE
@SchemaName VARCHAR(MAX) = N'',
@TableName VARCHAR(MAX) = N''

/*
	Variables Definition
	@count          -> Auxiliar variable to count the steps
	@columnCount    -> Store the number of columns in a table
	@columnName     -> Store the column name from a table
	@sqlDataType    -> Store the column data type
	@csharpDataType -> Store the equivalent data type to C#
	@defaultValue   -> Store the default value for the conversion
	@propertySet    -> Store the constructed property
	@setter         -> Store the constructed setter
	@conversion     -> Store the constructed conversion
	@convertTo      -> Store the proper conversion to C# data
*/

DECLARE
@count INT = 1
,@columnCount INT = 0
,@columnName VARCHAR(MAX) = NULL
,@sqlDataType VARCHAR(MAX) = NULL
,@csharpDataType VARCHAR(MAX) = NULL
,@defaultValue VARCHAR(MAX) = NULL
,@propertySet VARCHAR(MAX) = NULL
,@setter VARCHAR(MAX) = NULL
,@conversion VARCHAR(MAX) = NULL
,@convertTo VARCHAR(MAX) = NULL

/*
	Variable Tables
	@DataTypesTable   -> Store the data types' values from a table
	@PropertiesTable  -> Store the properties for the C# class
	@SettersTable     -> Store the setters for the constructor method
	@ConversionTable  -> Store the conversion data from SQL to C#
	@ConstructorTable -> Store the parameters for the constructor
*/
DECLARE @DataTypesTable TABLE
(
	Position INT,
	ColumnName VARCHAR(MAX),
	DataType VARCHAR(MAX)
)

DECLARE @PropertiesTable TABLE
(
	PROPERTY VARCHAR(MAX)
)

DECLARE @SettersTable TABLE
(
	SETTER VARCHAR(MAX)
)

DECLARE @ConversionTable TABLE
(
	Conversion VARCHAR(MAX)
)

DECLARE @ConstructorTable TABLE
(
	Construct VARCHAR(MAX)
)
		
-- Columns Count --
SET @columnCount = (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @SchemaName AND TABLE_NAME = @TableName);

-- Validate if there are columns to read --
IF ISNULL(@columnCount,0) != 0
BEGIN
	-- Insert data types -- 
	INSERT INTO @DataTypesTable 
	SELECT 
		ORDINAL_POSITION,
		COLUMN_NAME,
		DATA_TYPE 
	FROM 
		INFORMATION_SCHEMA.COLUMNS 
	WHERE 
		TABLE_SCHEMA = @SchemaName 
		AND 
		TABLE_NAME = @TableName

	WHILE(@count <= @columnCount)
	BEGIN
		SELECT 
			@columnName = DT.COLUMNNAME, 
			@sqlDataType = DT.DATATYPE 
		FROM 
			@DataTypesTable DT 
		WHERE 
			DT.POSITION = @count
		
		SET @csharpDataType = 
		CASE @sqlDataType
			WHEN 'bigint'           THEN 'long'
			WHEN 'binary'           THEN 'byte[]'
			WHEN 'bit'              THEN 'bool'
			WHEN 'char'             THEN 'string'
			WHEN 'date'             THEN 'DateTime'
			WHEN 'datetime'         THEN 'DateTime'
			WHEN 'decimal'          THEN 'decimal'
			WHEN 'varbinary(MAX)'   THEN 'byte[]'
			WHEN 'float'            THEN 'double'
			WHEN 'image'            THEN 'byte[]'
			WHEN 'int'              THEN 'int'
			WHEN 'money'            THEN 'decimal'
			WHEN 'nchar'            THEN 'string'
			WHEN 'ntext'            THEN 'string'
			WHEN 'numeric'          THEN 'decimal'
			WHEN 'nvarchar'         THEN 'string'
			WHEN 'real'             THEN 'float'
			WHEN 'rowversion'       THEN 'byte[]'
			WHEN 'smalldatetime'    THEN 'DateTime'
			WHEN 'smallint'         THEN 'int'
			WHEN 'smallmoney'       THEN 'decimal'
			WHEN 'sql_variant'      THEN 'object'
			WHEN 'text'             THEN 'string'
			WHEN 'time'             THEN 'TimeSpan'
			WHEN 'timestamp'        THEN 'byte[]'
			WHEN 'tinyint'          THEN 'byte'
			WHEN 'uniqueidentifier' THEN 'Guid'
			WHEN 'varchar'          THEN 'string'
			WHEN 'xml'              THEN 'SqlXml'
		END
		
		SET @convertTo = 
		CASE @csharpDataType
			WHEN 'long'     THEN CONCAT('Convert.ToInt64(Reader["',@columnName,'"]);')
			WHEN 'int'      THEN CONCAT('Convert.ToInt32(Reader["',@columnName,'"]);')
			WHEN 'string'   THEN CONCAT('Convert.ToString(Reader["',@columnName,'"]);')
			WHEN 'bool'     THEN CONCAT('Convert.ToBoolean(Reader["',@columnName,'"]);')
			WHEN 'double'   THEN CONCAT('Convert.ToDouble(Reader["',@columnName,'"]);')
			WHEN 'decimal'  THEN CONCAT('Convert.ToDecimal(Reader["',@columnName,'"]);')
			WHEN 'float'    THEN CONCAT('Convert.ToSingle(Reader["',@columnName,'"]);')
			WHEN 'object'   THEN CONCAT('Convert.ChangeType(Reader["',@columnName,'"],TypeCode.Object);')
			WHEN 'Guid'     THEN CONCAT('Reader.GetGuid(',@count,');')
			WHEN 'byte[]'   THEN CONCAT('Encoding.ASCII.GetBytes(Reader["',@columnName,'"]);')
			WHEN 'DateTime' THEN CONCAT('Convert.ToDateTime(Reader["',@columnName,'"]);')
			WHEN 'TimeSpan' THEN CONCAT('Reader.GetTimeSpan(',@count,');')
			WHEN 'SqlXml'   THEN CONCAT('Reader.GetSqlXml(',@count,');')
		END

		SET @defaultValue =
		CASE @csharpDataType
			WHEN 'long'     THEN '0'
			WHEN 'int'      THEN '0'
			WHEN 'string'   THEN '""'
			WHEN 'bool'     THEN 'false'
			WHEN 'double'   THEN '0.0'
			WHEN 'decimal'  THEN '0'
			WHEN 'float'    THEN '0.0f'
			WHEN 'object'   THEN '""'
			WHEN 'Guid'     THEN 'new Guid()'
			WHEN 'byte[]'   THEN 'null'
			WHEN 'DateTime' THEN 'DateTime.Now'
			WHEN 'TimeSpan' THEN 'TimeSpan.Zero'
			WHEN 'SqlXml'   THEN 'SqlXml.Null'	
		END
		
		-- Class Properties --
		SET @propertySet = CONCAT('public ', @csharpDataType, ' ', @columnName, ' { get; set; }');
		INSERT INTO @PropertiesTable(PROPERTY) VALUES (@propertySet);

		-- Class Constructor --
		INSERT INTO @ConstructorTable(CONSTRUCT) VALUES(CONCAT(@csharpDataType, ' ', @columnName));

		-- Constructor Setters --
		SET @setter = CONCAT('this.',@columnName,' = ', @columnName, ';');
		INSERT INTO @SettersTable(SETTER) VALUES(@setter);

		-- Conversion --
		SET @conversion = CONCAT('model.', @columnName, ' = Reader["', @columnName, '"] == DBNull.Value ? ',@defaultValue,' : ', @convertTo);
		INSERT INTO @ConversionTable(CONVERSION) VALUES (@conversion);

		PRINT CONCAT(@count, ': ', @sqlDataType, ' => ', @csharpDataType);

		SET @count += 1
	END
END	
ELSE
BEGIN
	PRINT 'DATA VALUE IS 0 OR NULL'
END

-- Class Properties --
SELECT * FROM @PropertiesTable;

-- Class Constructor --
SELECT (STUFF((SELECT ', ' + CONSTRUCT FROM @ConstructorTable FOR XML PATH('')),1,2,'')) AS 'CONSTRUCTOR';

-- Constructor Setters --
SELECT * FROM @SettersTable;
	
-- Conversion --
SELECT * FROM @ConversionTable;	