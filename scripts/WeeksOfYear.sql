/*
    Name: WeeksOfYear.sql
    Description: Returns the weeks within a year
    Keywords: dates
*/

;WITH Dates AS
(
    SELECT    
		DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0) AS YearStartDate,
		DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) + 1, -1) AS YearEndDate
)
, Weeks AS
(
    SELECT
		1 AS WeekID, 
		YearStartDate AS StartDate, 
		CASE 
			WHEN DATEPART(WEEKDAY, YearEndDate) = 52 THEN DATEADD(d, (8 - datepart(WEEKDAY, YearStartDate)), YearStartDate) ELSE DATEADD(d, (7 - datepart(WEEKDAY, YearStartDate)), YearStartDate) 
		END AS EndDate
    FROM
		Dates
    UNION ALL
    SELECT
		WeekID + 1, 
		DATEADD(d, 1, EndDate),
		CASE 
			WHEN DATEADD(d, 7, EndDate) <= (SELECT YearEndDate from Dates) THEN DATEADD(d, 7, EndDate) ELSE (SELECT YearEndDate from Dates) 
		END
    FROM
		Weeks
    WHERE
		EndDate < (SELECT YearEndDate FROM Dates)
)

SELECT
	WeekID, 
	CONCAT(REPLACE(CONVERT(VARCHAR(20), StartDate, 111),'/','-'),' ~ ',REPLACE(CONVERT(VARCHAR(20), EndDate, 111),'/','-')) AS WeekDate
FROM
	Weeks
WHERE
	EndDate <= DATEADD(DAY, 7, GETDATE())