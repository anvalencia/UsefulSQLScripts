/*
    Name: DatesFromDateRange.sql
    Description: Returns all dates within a date range
    Keywords: dates
    Ref: https://stackoverflow.com/questions/23290454/get-all-dates-between-two-dates-in-sql-server
*/

DECLARE 
@MinDate DATE = '2017-12-24',
@MaxDate DATE = '2018-01-20';

SELECT TOP (DATEDIFF(DAY, @MinDate, @MaxDate) + 1)
	Date = DATEADD(DAY, ROW_NUMBER() OVER(ORDER BY a.object_id) - 1, @MinDate)
FROM 
	sys.all_objects a
	CROSS JOIN 
	sys.all_objects b;