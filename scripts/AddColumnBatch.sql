/*
    Name: AddColumnBatch.sql
    Description: Adds a column to multiple tables
    Keywords: batch, columns, tables
    Ref: 
*/

DECLARE
@tableFilter NVARCHAR(MAX) = N'',
@columnName NVARCHAR(MAX) = N'',
@columnType NVARCHAR(MAX) = N'',
@cmd NVARCHAR(MAX) = N''

IF(@tableFilter = '')
BEGIN
    RETURN 'Table name not specified'
END

IF(@columnName = '')
BEGIN
    RETURN 'Column name not specified'
END

IF(@columnType = '')
BEGIN
    RETURN 'Data type not specified'
END

SELECT @cmd += '
	ALTER TABLE ' + CONCAT(QUOTENAME(a.TABLE_CATALOG),'.',QUOTENAME(a.TABLE_SCHEMA),'.',QUOTENAME(a.TABLE_NAME)) + ' ADD ' + @columnName + ' ' + @columnType
FROM
	INFORMATION_SCHEMA.TABLES a
	JOIN
	sys.tables b ON b.object_id = OBJECT_ID(a.TABLE_NAME)
WHERE
	a.TABLE_NAME LIKE '%' + tableFilter + '%'
	AND
	b.type = 'U'
	AND
	b.object_id NOT IN (SELECT object_id FROM sys.columns WHERE name = @columnName)
ORDER BY
	a.TABLE_NAME

EXEC(@cmd)