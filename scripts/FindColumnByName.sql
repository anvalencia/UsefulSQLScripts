/*
    Name: FindColumnByName.sql
    Description: Returns a list of tables that contain a column with some word
    Keywords: filtering, tables
    Ref: 
*/

DECLARE
@filter NVARCHAR(MAX) = N'',
@Procedure NVARCHAR(MAX) = N'',
@items INT = 0,
@count INT = 0

DECLARE @tables TABLE
(
    Position INT,
    NAME NVARCHAR(MAX)
)

DECLARE @ProceduresTable TABLE
(
    TableName      NVARCHAR(MAX),
    ColumnName     NVARCHAR(MAX), 
    DataType       NVARCHAR(MAX),
    DataSize       INT, 
    DataPrecision  FLOAT,
    Scale          FLOAT,
    Collation      NVARCHAR(MAX)
)

SET @items = (SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES);
INSERT INTO
	@tables
SELECT
	ROW_NUMBER() OVER (ORDER BY RT.TABLE_NAME) AS 'Position',CONCAT(RT.TABLE_SCHEMA,'.',RT.TABLE_NAME) AS NAME
FROM 
	INFORMATION_SCHEMA.TABLES RT

WHILE(@count < @items)
BEGIN
	SET @Procedure = (SELECT NAME FROM @tables WHERE Position = @count)

	INSERT INTO
		@ProceduresTable
	SELECT
		@Procedure,
		name,
		type_name(user_type_id),
		max_length,
		CASE 
            WHEN type_name(system_type_id) = 'uniqueidentifier' THEN PRECISION ELSE ODBCPREC(system_type_id,max_length,PRECISION) 
        END,
		ODBCSCALE(system_type_id,scale),
		CONVERT(sysname, CASE WHEN system_type_id IN (35,99,167,175,231,239) THEN ServerProperty('collation') END )
	FROM
		sys.columns
	WHERE
		OBJECT_ID = OBJECT_ID(@Procedure);	

	SET @count += 1;
END

SELECT * FROM @ProceduresTable WHERE ColumnName LIKE '%' + @filter + '%' ORDER BY TableName