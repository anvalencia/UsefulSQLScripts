/*
    Name: SearchProcedureDefinition.sql
    Description: Search a string/keyword inside every stored procedure
    Keywords: stored procedures, filtering
    Ref: 
*/

DECLARE 
@string NVARCHAR(MAX) = N''

SELECT
	b.name AS SchemaName,
	a.name AS ProcedureName,
	a.modify_date AS ModifyDate,
	OBJECT_DEFINITION(a.object_id) AS ProcedureDefinition
FROM
	sys.procedures a
	JOIN
	sys.schemas b ON b.schema_id = a.schema_id
WHERE
	OBJECT_DEFINITION(a.object_id) LIKE '%' + @string + '%'
ORDER BY
	a.modify_date DESC