/*
	References: 
	https://www.sqlservercentral.com/Forums/604108/List-of-scheduled-sql-jobs?PageIndex=%5B0%5D
	https://docs.microsoft.com/en-us/sql/relational-databases/system-stored-procedures/sp-help-jobschedule-transact-sql?view=sql-server-2017
	https://www.mssqltips.com/sqlservertip/4921/queries-to-inventory-your-sql-server-agent-jobs/
*/
SELECT DISTINCT
	substring(a.name,1,100) AS [Job Name],
	'Enabled' =
	CASE
		WHEN a.enabled = 0 THEN 'No'
		WHEN a.enabled = 1 THEN 'Yes'
	END,
	substring(b.name,1,30) AS [Name of the schedule],
	'Frequency of the schedule execution' =
	CASE
		WHEN b.freq_type = 1 THEN 'Once'
		WHEN b.freq_type = 4 THEN 'Daily'
		WHEN b.freq_type = 8 THEN 'Weekly'
		WHEN b.freq_type = 16 THEN 'Monthly'
		WHEN b.freq_type = 32 THEN 'Monthly relative'
		WHEN b.freq_type = 32 THEN 'Execute when SQL Server Agent starts'
	END,
	'Units for the freq_subday_interval' =
	CASE
		WHEN b.freq_subday_type = 1 THEN 'At the specified time'
		WHEN b.freq_subday_type = 2 THEN 'Seconds'
		WHEN b.freq_subday_type = 4 THEN 'Minutes'
		WHEN b.freq_subday_type = 8 THEN 'Hours'
	END,
	cast(cast(b.active_start_date as varchar(15)) as datetime) as active_start_date,
	cast(cast(b.active_end_date as varchar(15)) as datetime) as active_end_date,
	cast(cast(c.next_run_date as varchar(15)) as datetime) as next_run_date,
	Stuff(Stuff(right('000000'+Cast(c.next_run_time as Varchar),6),3,0,':'),6,0,':') as Run_Time,
	b.date_created
FROM
	msdb..sysjobhistory d
	INNER JOIN
	msdb..sysjobs a ON a.job_id = d.job_id
	INNER JOIN
	msdb..sysJobschedules c ON a.job_id = c.job_id
	INNER JOIN
	msdb..SysSchedules b on b.Schedule_id=c.Schedule_id

SELECT
	database_name,
	name
	,CONVERT(VARCHAR(16), date_created, 120) date_created
	,sysjobsteps.step_id
	,sysjobsteps.step_name
	,LEFT(CAST(sysjobsteps.last_run_date AS VARCHAR),4)+ '-' + SUBSTRING(CAST(sysjobsteps.last_run_date AS VARCHAR),5,2) + '-' + SUBSTRING(CAST(sysjobsteps.last_run_date AS VARCHAR),7,2) last_run_date
	,CASE 
		WHEN LEN(CAST(sysjobsteps.last_run_time AS VARCHAR)) = 6 THEN SUBSTRING(CAST(sysjobsteps.last_run_time AS VARCHAR),1,2) + ':' + SUBSTRING(CAST(sysjobsteps.last_run_time AS VARCHAR),3,2) + ':' + SUBSTRING(CAST(sysjobsteps.last_run_time AS VARCHAR),5,2)
		WHEN LEN(CAST(sysjobsteps.last_run_time AS VARCHAR)) = 5 THEN '0' + SUBSTRING(CAST(sysjobsteps.last_run_time AS VARCHAR),1,1) + ':'+SUBSTRING(CAST(sysjobsteps.last_run_time AS VARCHAR),2,2) + ':' + SUBSTRING(CAST(sysjobsteps.last_run_time AS VARCHAR),4,2)
		WHEN LEN(CAST(sysjobsteps.last_run_time AS VARCHAR)) = 4 THEN '00:' + SUBSTRING(CAST(sysjobsteps.last_run_time AS VARCHAR),1,2) + ':' + SUBSTRING(CAST(sysjobsteps.last_run_time AS VARCHAR),3,2)
		WHEN LEN(CAST(sysjobsteps.last_run_time AS VARCHAR)) = 3 THEN '00:' + '0' + SUBSTRING(CAST(sysjobsteps.last_run_time AS VARCHAR),1,1) + ':' + SUBSTRING(CAST(sysjobsteps.last_run_time AS VARCHAR),2,2)
		WHEN LEN(CAST(sysjobsteps.last_run_time AS VARCHAR)) = 2 THEN '00:00:' + CAST(sysjobsteps.last_run_time AS VARCHAR) 
		WHEN LEN(CAST(sysjobsteps.last_run_time AS VARCHAR)) = 1 THEN '00:00:' + '0' + CAST(sysjobsteps.last_run_time AS VARCHAR)
	END last_run_time
	,LEFT(CAST(sysjobschedules.next_run_date AS VARCHAR),4) + '-' + SUBSTRING(CAST(sysjobschedules.next_run_date AS VARCHAR),5,2) + '-' + SUBSTRING(CAST(sysjobschedules.next_run_date AS VARCHAR),7,2) next_run_date
	,CASE 
		WHEN LEN(CAST(next_run_time AS VARCHAR)) = 6 THEN SUBSTRING(CAST(next_run_time AS VARCHAR),1,2) + ':' + SUBSTRING(CAST(next_run_time AS VARCHAR),3,2) + ':' + SUBSTRING(CAST(next_run_time AS VARCHAR),5,2) 
		WHEN LEN(CAST(next_run_time AS VARCHAR)) = 5 THEN '0' + SUBSTRING(CAST(next_run_time AS VARCHAR),1,1) + ':' + SUBSTRING(CAST(next_run_time AS VARCHAR),2,2) + ':' + SUBSTRING(CAST(next_run_time AS VARCHAR),4,2)
		WHEN LEN(CAST(next_run_time AS VARCHAR)) = 4 THEN '00:' + SUBSTRING(CAST(next_run_time AS VARCHAR),1,2) + ':' + SUBSTRING(CAST(next_run_time AS VARCHAR),3,2)
		WHEN LEN(CAST(next_run_time AS VARCHAR)) = 3 THEN '00:' + '0' + SUBSTRING(CAST(next_run_time AS VARCHAR),1,1) + ':' + SUBSTRING(CAST(next_run_time AS VARCHAR),2,2)
		WHEN LEN(CAST(next_run_time AS VARCHAR)) = 2 THEN '00:00:' + CAST(next_run_time AS VARCHAR) 
		WHEN LEN(CAST(next_run_time AS VARCHAR)) = 1 THEN '00:00:' + '0' + CAST(next_run_time AS VARCHAR)
	END next_run_time
FROM 
	msdb.dbo.sysjobs
	LEFT JOIN 
	msdb.dbo.sysjobschedules ON sysjobs.job_id = sysjobschedules.job_id
	INNER JOIN 
	msdb.dbo.sysjobsteps ON sysjobs.job_id = sysjobsteps.job_id
WHERE
	enabled = 1
	and
	database_name in ('FwReports','INDs')
ORDER BY
	database_name,
	name,
	sysjobsteps.step_id