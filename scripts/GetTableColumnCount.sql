/*
    Name: GetTableColumnCount.sql
    Description: Returns a list of the columns count for each table
    Keywords: tables, columns, aggregate
    Ref: 
*/

DECLARE
@count INT = 1,
@tableCount  INT = 0,
@TableSchema VARCHAR(MAX),
@TableName   VARCHAR(MAX),
@ColumnCount INT = 0

DECLARE @tables TABLE
(
	Position INT,
	TableSchema VARCHAR(MAX),
	TableName VARCHAR(MAX)
)

DECLARE @columnsTable TABLE
(
	SchemaName VARCHAR(100),
	TableName VARCHAR(150),
	ColumnCount INT
)

SET @tableCount = 
(
	SELECT 
		COUNT(*) 
	FROM 
		INFORMATION_SCHEMA.TABLES 
	WHERE 
		TABLE_TYPE = 'BASE TABLE'
)

INSERT INTO 
	@tables 
SELECT 
	ROW_NUMBER() OVER (ORDER BY TABLE_NAME) AS 'Position',
	TABLE_SCHEMA,
	TABLE_NAME 
FROM 
	INFORMATION_SCHEMA.TABLES 
WHERE 
	TABLE_TYPE = 'BASE TABLE';

WHILE(@count < @tableCount)
BEGIN
	SELECT 
		@TableSchema = TableSchema,
		@TableName = TableName
	FROM 
		@tables 
	WHERE 
		Position = @count
	
	SELECT 
		@ColumnCount = COUNT(*) 
	FROM 
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME   = @TableName;
	
	INSERT INTO @columnsTable VALUES (@TableSchema, @TableName, @ColumnCount)
	
	SET @count += 1
END

SELECT * FROM @columnsTable ORDER BY SchemaName