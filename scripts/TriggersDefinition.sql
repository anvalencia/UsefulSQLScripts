/*
    Name: TriggersDefinition.sql
    Description: Get all triggers information
    Keywords: triggers
    Ref: 
*/

SELECT
   ServerName   = @@servername,
   DatabaseName = db_name(),
   SchemaName   = isnull( s.name, '' ),
   TableName    = isnull( o.name, 'DDL Trigger' ),
   TriggerName  = t.name, 
   Defininion   = object_definition( t.object_id )
FROM 
	sys.triggers t
	LEFT JOIN 
	sys.all_objects o ON t.parent_id = o.object_id
	LEFT JOIN 
	sys.schemas s ON s.schema_id = o.schema_id
ORDER BY 
   SchemaName,
   TableName,
   TriggerName