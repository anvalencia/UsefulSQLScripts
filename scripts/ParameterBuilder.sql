/*
    Name: ParameterBuilder.sql
    Description: Generates a list of the parameters needed by a stored procedure and convert it to C# syntax
    Keywords: stored procedures, parameters, c#
    Ref: 
*/

DECLARE
@spName NVARCHAR(MAX) = N''

SELECT
	CONCAT('parameters.Add(new SqlParameter("',a.name,'",',REPLACE(a.name,'@',''),'));') AS ParametersNames
FROM
	sys.parameters a
	JOIN
	sys.procedures b on b.object_id = a.object_id
WHERE
	OBJECT_NAME(b.object_id) = @spName
ORDER BY
	a.parameter_id