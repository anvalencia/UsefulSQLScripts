/*
    Name: FindProcedureByParameterName.sql
    Description: Returns a list of procedures that contain a parameter with some word
    Keywords: filtering, procedures
    Ref: 
*/

DECLARE 
@ProcedureName NVARCHAR(MAX) = N'',
@items INT = 0,
@count INT = 0

DECLARE @Procedures TABLE
(
    Position INT,
    Name NVARCHAR(MAX)
)

DECLARE @ProceduresTable TABLE
(
    ProcedureName  NVARCHAR(MAX),
    ParamName      NVARCHAR(MAX), 
    DataType       NVARCHAR(MAX),
    ParamLength    INT, 
    ParamPrecision FLOAT,
    Scale          FLOAT,
    ParamID        INT,
    Collation      NVARCHAR(MAX),
    IsReadonly     NVARCHAR(MAX)
)

SET @items = (SELECT COUNT(*) FROM INFORMATION_SCHEMA.ROUTINES);
INSERT INTO 
	@Procedures
SELECT
	ROW_NUMBER() OVER (ORDER BY RT.SPECIFIC_NAME) AS 'Position',CONCAT(RT.SPECIFIC_SCHEMA,'.',RT.SPECIFIC_NAME) AS NAME
FROM 
	INFORMATION_SCHEMA.ROUTINES RT;

WHILE(@count < @items)
BEGIN
	SET @ProcedureName = (SELECT Name FROM @Procedures WHERE POSITION = @count);

	INSERT INTO
		@ProceduresTable
	SELECT
		@ProcedureName,
		name,
		type_name(user_type_id),
		max_length,
		CASE 
            WHEN type_name(system_type_id) = 'uniqueidentifier' THEN PRECISION ELSE ODBCPREC(system_type_id,max_length,PRECISION) 
        END,
		ODBCSCALE(system_type_id,scale),
		parameter_id,
		CONVERT(sysname, CASE WHEN system_type_id IN (35,99,167,175,231,239) THEN ServerProperty('collation') END ),
		CASE WHEN is_readonly = 1 THEN 'YES' ELSE 'NO' END
	FROM
		sys.parameters
	WHERE
		OBJECT_ID = OBJECT_ID(@ProcedureName);	

	SET @count += 1;
END

SELECT * FROM @ProceduresTable ORDER BY ProcedureName;